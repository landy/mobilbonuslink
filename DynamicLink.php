<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jiril_000
 * Date: 21.6.13
 * Time: 13:15
 * To change this template use File | Settings | File Templates.
 */

class DynamicLink
{

    /**
     * @var ILinkStrategy
     */
    private $strategy;

    public function __construct(ILinkStrategy $strategy)
    {
        $this->strategy = $strategy;
    }

    public function forge($params)
    {
        return $this->strategy->generateUrl($params);
    }
}