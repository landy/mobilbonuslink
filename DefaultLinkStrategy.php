<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jiril_000
 * Date: 21.6.13
 * Time: 13:18
 * To change this template use File | Settings | File Templates.
 */

require_once("ILinkStrategy.php");

class DefaultLinkStrategy implements ILinkStrategy {

    public function generateUrl($params)
    {
        return "http://foo.com";
    }
}