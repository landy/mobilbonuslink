<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jiril_000
 * Date: 21.6.13
 * Time: 13:14
 * To change this template use File | Settings | File Templates.
 */

class Link
{

    /**
     * @var DynamicLink
     */
    static $dynamiCLink;

    public static function forge($params)
    {
        return self::$dynamiCLink->forge($params);
    }
}