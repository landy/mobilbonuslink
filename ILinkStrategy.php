<?php
/**
 * Created by JetBrains PhpStorm.
 * User: jiril_000
 * Date: 21.6.13
 * Time: 13:16
 * To change this template use File | Settings | File Templates.
 */

interface ILinkStrategy {
    public function generateUrl($params);
}